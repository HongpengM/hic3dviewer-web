from flask import Flask
from flask_cors import CORS
from flask import send_file, make_response
import numpy as np
import scipy as sp
import scipy.misc as smisc

app = Flask(__name__)
CORS(app)

@app.route("/")
def helloWorld():
  return "Hello, cross-origin-world!"

@app.route('/testImage')
def testImg():
    mat = np.load('./K562_counts2.npy')
    np.interp(mat, (mat.min(), mat.max()), (0, 255))
    smisc.toimage(mat, cmin=0.0, cmax=...).save('test.jpg')

    print(mat.shape)
    return 'test png' + str(mat.shape)
