import sys
sys.path.append('..')
import numpy as np
from scipy import sparse
from server.resource_map.domain.loader import Matrix3ColumnsLoader

loader = Matrix3ColumnsLoader()
loader.load_data('../data/SRR400252_500000_iced.matrix')
class HiCTiledMatrix(object):
    """Documentation for HiCTiledMatrix

    """
    def __init__(self):
        super(HiCTiledMatrix, self).__init__()
        
    def fromText(self, path):
        loader = Matrix3ColumnsLoader()
        loader.load_data(path)
        self.data = loader.data
 
print(loader.data.shape)
