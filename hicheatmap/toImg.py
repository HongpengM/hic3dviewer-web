import numpy as np
import scipy as sp
import scipy.misc as misc
import spectra

class Scale(object):
    """Documentation for Scale

    """
    def __init__(self):
        super(Scale, self).__init__()

    def domains(x,y):
        this.domain = [x,y]
        return this

    def range(x, y):
        this.range = [x,y]
        return this

    def to(x):
        y = (x - this.domain[0])/(this.domain[1] - this.domain[0])
        y = y * (this.range[1] - this.range[0]) + this.range[0]
        
        

    

mat = sp.sparse.load_npz('./SRR400252_500000_sparse.npz')
zoom_factor = 10
a,b = np.array(mat.shape)/zoom_factor
print(a,b)
print(mat.shape)
mat_p = mat[0:100, 0:100]
# mat_p = np.log10(mat[0:100,0:100])
# mat_p[mat_p == -np.inf] = 0
print(mat_p)
print(np.interp(mat_p,(mat_p.min(), mat_p.max()),(0,1) ))
print(mat_p.min(), mat_p.max())
