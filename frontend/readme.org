* Run frontend dev

#+BEGIN_SRC sh
npm i --save-dev webpack webpack-dev-server webpack-cli three
npm run dev
open http://localhost:7777
#+END_SRC


* Dev servers

#+BEGIN_SRC sh
npx webpack-dev-server
http-server --cors  -a localhost
#+END_SRC
